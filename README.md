# README #

ReactJS (JSX) - Electron Boilerplate

You have to install some [builder package](https://github.com/electron/electron/blob/master/docs/tutorial/application-distribution.md#packaging-tools), if you would distribute your application.

### START ###

```sh
$ cd source
$ yarn init
$ yarn install
$ cd ..
$ cd public
$ yarn init
$ yarn install
```

### DEV SERVER ###

```sh
$ cd source
$ yarn build-dev
```

```sh
$ cd public
$ yarn start
```

### PRODUCTION SERVER ###

```sh
$ cd source
$ yarn build-prod
```

```sh
$ cd public
$ yarn start
```