import React from 'react';
import { render } from 'react-dom';

import AspectRatioCalulator from './app/lib/aspectRatioCalulator'
import Config from './config'
import Face from './app/models/face';
import FaceAPI from './app/lib/faceapi';
import ImageSizeGetter from './app/lib/imageSizeGetter';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			faces: [],
			imgUrl: 'http://dreamicus.com/data/face/face-04.jpg'
		};
	}
	getFaceApi () {
		ImageSizeGetter(this.state.imgUrl, (error, imageData) => {
			if (error !== null) {
				console.log(error);
			} else {
				let imageRatio  = AspectRatioCalulator(imageData.width, imageData.height, 500, 300);
				FaceAPI(this.state.imgUrl, (error, response) => {
					if (error !== null) {
						error.then(value => console.log(value));
					} else {
						let faces = [];
						response.forEach(function(value) {
							faces.push(new Face(
								value.faceAttributes.headPose.roll,
								value.faceAttributes.headPose.yaw,
								value.faceLandmarks,
								value.faceRectangle,
								imageRatio
							));
						}, this);
						this.setState({ faces: faces });
					}
				});
			}
		});
	}
	render () {
		const divs = this.state.faces.map((value, key) => (
			<div style={{
				position: 'absolute',
				left: value.getFaceRectangle().left,
				top: value.getFaceRectangle().top,
				width: value.getFaceRectangle().width,
				height: value.getFaceRectangle().height,
				transform: `rotateZ(${value.getFaceRoll()}deg) rotateY(${value.getFaceYaw()}deg)`
			}} key={ key } dangerouslySetInnerHTML={{ __html: Config.GLASSES }}/>
		));

		return <div style={{
			width: 500, height: 336, display: 'flex', 
			flexDirection: 'column', overflow: 'hidden' }}>
			<div style={{ width: 500, height: 300 }}>
				<img style={{ maxHeight: 300 }} src={`${this.state.imgUrl}`}
					alt='face' onLoad={ this.getFaceApi.bind(this) } />
				{ divs }
			</div>
			<div style={{ width: 500, height: 36 }}>
				<input style={{ width: 500, height: 36, fontSize: 30 }} type='url' 
					value={ this.state.imgUrl } onChange={ event => {
						this.setState({ imgUrl: event.target.value });
					}
				}/>
			</div>
		</div>
	}
}

render(<App/>, document.getElementById('root'));
