const FACE_API_URL = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect'
const SUBSCRIPTION_KEY = 'fafb029adcaa44de92e1d1c1db9e2281';
const GLASSES = require('-!svg-inline-loader!./assets/images/glasses.svg');
const FACE_API_PARAMS = {
    'returnFaceId': 'false',
    'returnFaceLandmarks': 'true',
    'returnFaceAttributes': 'headPose'
}

const FACE_API_BODY_PARAMS = (imgUrl) => {
    return {
        'url': `${imgUrl}`,
    }
}

export default {
    FACE_API_PARAMS,
    FACE_API_URL,
    SUBSCRIPTION_KEY,
    FACE_API_BODY_PARAMS,
    GLASSES
}