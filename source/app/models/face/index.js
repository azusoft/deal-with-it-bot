export default class Face {
	constructor(faceRoll, faceYaw, faceLandmarks, faceRectangle, ratio) {
		this.faceRoll = faceRoll || null;
		this.faceYaw = faceYaw || null;
		this.faceRectangle = faceRectangle || null;

		let centerOfEyesX = ((faceLandmarks.pupilLeft.x + faceLandmarks.pupilRight.x) / 2);
		let centerOfEyesY = ((faceLandmarks.pupilLeft.y + faceLandmarks.pupilRight.y) / 2);

        
        if (this.faceRectangle != null) {
			this.faceRectangle.left = centerOfEyesX - this.faceRectangle.width * 0.72;
			this.faceRectangle.top = centerOfEyesY - this.faceRectangle.height * 0.1;
			this.faceRectangle.width *= 1.2;
			this.faceRectangle.height *= 1.2;


			this.faceRectangle.left *= ratio;
			 this.faceRectangle.top *= ratio;
			this.faceRectangle.width *= ratio;
			this.faceRectangle.height *= ratio;
    	}
	}

	getFaceRoll () {
		return this.faceRoll;
	}

	getFaceYaw () {
		return this.faceYaw;
	}

	getFaceRectangle () {
		return this.faceRectangle;
	}
}