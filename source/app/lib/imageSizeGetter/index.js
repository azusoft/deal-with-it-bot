export default (imgUrl, callback) => {
    let img = new Image();
    img.onload = (event) => {
        callback(null, { width: event.path[0].width, height: event.path[0].height });
    }
    img.onerror = (event) => callback(new Error('Wrong image URL'));
    img.src = imgUrl;
}