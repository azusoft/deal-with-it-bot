/**
* @param {Number} srcWidth width of source image
* @param {Number} srcHeight height of source image
* @param {Number} maxWidth maximum available width
* @param {Number} maxHeight maximum available height
* @return {Number} ratio
*/

export default (srcWidth, srcHeight, maxWidth, maxHeight) => {
	let ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    return ratio;
}