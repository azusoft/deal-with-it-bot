import Config from '../../../config';
import UrlEncoder from '../urlencode';

export default (imgUrl, callback) => {
    fetch(`${Config.FACE_API_URL}?${UrlEncoder(Config.FACE_API_PARAMS)}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': Config.SUBSCRIPTION_KEY
        },
        body: JSON.stringify(Config.FACE_API_BODY_PARAMS(`${imgUrl}`))
    }).then((response) => {
        response.json().then(values => {
            if (values.error !== undefined) {
                callback(new Error(values.error.message));
            } else {
                callback(null, values)
            }
        });
    }).catch((error) => {
        callback(new Error(error));
    });
}